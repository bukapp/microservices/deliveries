import express, { Express, Request, Response, Router, NextFunction } from 'express';
import middleware from '../modules/middleware';
import {DeliveriesRepository} from '../models/Deliveries'
import axios from 'axios';

var router: Router = express.Router()

/**** SECURE ZONE ****/
router.use(middleware)

/* GET all deliveries */
router.get('/', async function(req: Request, res: Response, next: NextFunction) {
    const deliveries = await DeliveriesRepository.find();
    if(deliveries != null){
        res.send(deliveries);
    }        
    else{
        res.sendStatus(404);
    }
});

/* POST a delivery */
router.post('/', async function(req: Request, res: Response, next: NextFunction) {
    const post = new DeliveriesRepository({
        client: req.body.client,
        command: req.body.command,
        status: req.body.status,
        date: new Date().toISOString(),
	})
	await post.save()
	res.send(post)
});

/* GET a delivery */
router.get('/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
        const deliveries = await DeliveriesRepository.findOne({ _id: req.params.id });
        if(deliveries != null){
            if(req.body.user.id == deliveries.client || req.body.user.id == deliveries.deliverer || JSON.parse(req.body.user.role).includes("EMPLOYEE")){
                res.send(deliveries);
            }        
            else{
                res.sendStatus(403);
            }
        }else{
            res.sendStatus(404);
        }
    } catch (error){
        console.log(error)
        res.sendStatus(500)
    }
});
/* GET all command of a restaurant by user*/
router.get('/client/:id/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const delivery = await DeliveriesRepository.find({ client: req.params.id });
        if (req.body.user.id == req.params.id || JSON.parse(req.body.user.role).includes("EMPLOYEE")) {
            if (delivery != null) {
                res.send(delivery);
            }
            else {
                res.sendStatus(404);
            }
        } else {
            res.sendStatus(403);
        }
    } catch (error){
        console.log(error)
        res.status(500)
        res.send({ error: "Internal error" })
    }
});
/* GET all command of a restaurant by command id*/
router.get('/command/:id/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const delivery = await DeliveriesRepository.findOne({ command: req.params.id });
        const command: any = await axios.get(`http://apigateway/commands/${req.params.id}`, {
            headers: {
                'x-access-token': req.header('x-access-token') || ""
            }
        })
        const restaurant: any = await axios.get(`http://apigateway/restaurants/${command.data.restaurant}`, {
            headers: {
                'x-access-token': req.header('x-access-token') || ""
            }
        })

        if (req.body.user.id == restaurant.data.owner  || JSON.parse(req.body.user.role).includes("EMPLOYEE")) {
            if (delivery != null) {
                res.send(delivery);
            }
            else {
                res.sendStatus(404);
            }
        } else {
            res.sendStatus(403);
        }
    } catch (error){
        console.log(error)
        res.status(500)
        res.send({ error: "Internal error" })
    }
});

/* UPDATE a delivery */
router.put('/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
		const delivery = await DeliveriesRepository.findOne({ _id: req.params.id })
        if(req.body.user.id == delivery.deliverer || JSON.parse(req.body.user.role).includes("EMPLOYEE")){
            delivery.status = req.body.status || delivery.status;
            if(req.body.status == -1){
                delete delivery.deliverer
            }
            await delivery.save()
            res.send(delivery)
        }else{
            res.sendStatus(403);
        }
	} catch (error){
        console.log(error)
		res.status(404).send({ error: "Delivery doesn't exist!" })
    }
});

/* Handle a delivery */
router.get('/handle/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
		const delivery = await DeliveriesRepository.findOne({ _id: req.params.id })
        if(delivery.status == -1 ){
            delivery.deliverer = req.body.user.id;
            delivery.status = 0;
            await delivery.save()
            res.send(delivery)
        }else{
            res.sendStatus(403);
        }
	} catch (error){
        console.log(error)
		res.status(404).send({ error: "Delivery doesn't exist!" })
    }
});

export default router;
