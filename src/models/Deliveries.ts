const mongoose = require("mongoose")

const schema = mongoose.Schema({
    client: Number,
    command: String,
    deliverer: Number,
    status: Number,
    date: Date,
})

const repository = mongoose.model("Deliveries", schema)

export {repository as DeliveriesRepository}